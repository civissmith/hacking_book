section .data   ; data segment
msg     db  "Hello, World!", 0x0a   ; String and newline char

section .text   ; text segment
global _start   ; Default entry point for ELF linking

_start:

; SYSCALL: write(1, msg, 14)
mov eax, 4      ; Puts 4 into EAX since syscall is #4
mov ebx, 1      ; Puts 1 into EBX since stdout is 1
mov ecx, msg    ; Puts address of string into ECX
mov edx, 14     ; Put 14 into EDX since string is 14 bytes
int 0x80        ; Call the kernel to make the system call

; SYSCALL: exit(0)
mov eax, 1      ; Puts 1 into EAX since syscall is #1
mov ebx, 0      ; Puts 0 into EBX since return is 0
int 0x80        ; Call the kernel to execute the system call
